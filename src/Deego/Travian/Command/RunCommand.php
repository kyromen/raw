<?php

namespace Deego\Travian\Command;

use Symfony\Bundle\DoctrineMongoDBBundle\Command\DoctrineODMCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use Doctrine\ODM\MongoDB\DocumentManager;

use Behat\Mink\Mink;
use Behat\Mink\Driver\SahiDriver;
use Behat\Mink\Session;

use Deego\Travian\OutputWrapper\TimestampingOutputWrapper;

class RunCommand extends DoctrineODMCommand {
    protected function configure()
    {
        $this
            ->setName('deego:travian:run')
            ->addOption('browser-name', 'b', InputOption::VALUE_REQUIRED, 'Browser name for Sahi (chrome, firefox, phantomjs...)', 'chrome')
    ;}

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output = new TimestampingOutputWrapper($output);
        $mink = new Mink(array(
            'sahi' => new Session(new SahiDriver($input->getOption('browser-name')))
        ));
        $session = $mink->getSession('sahi');
        /** @var $dm \Doctrine\ODM\MongoDB\DocumentManager */
        $dm = $this->getContainer()->get('doctrine.odm.mongodb.document_manager');
        $accountRepository = $dm->getRepository('Deego\Travian\Document\Account');
        $accounts = $accountRepository->findAll();
        foreach ($accounts as $account /** @var \Deego\Travian\Document\Account $account */) {
            $output->writeln('Processing account "'.$account.'"');
            $session->visit('http://ts3.travian.ru/logout.php');
            $page = $session->getPage();
            $page->fillField('name', $account->getUsername());
            $page->fillField('password', $account->getPassword());
            $page->pressButton('Войти');
            $queue = $account->getQueue();
            $id = $queue[0];
            $session->visit('/build.php?id='.$id);
            $buildButton = $page->find('css', '.contractLink button');
            if ($buildButton) {
                $buildButton->press();
                $id = array_shift($queue);
                array_push($queue, $id);
                $account->setQueue($queue);
                $output->writeln('Building #'.$id);
            } else {
                $output->writeln('Skipping');
            }
            $dm->flush();
        }
    }

}
