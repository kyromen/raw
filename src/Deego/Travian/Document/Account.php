<?php

namespace Deego\Travian\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @MongoDB\Document(
 *      repositoryClass="Deego\Travian\Repository\AccountRepository"
 * )
 */
class Account
{
    /**
     * @MongoDB\Id(strategy="auto")
     */
    protected $id;

    /**
     * @MongoDB\String
     * @Assert\NotBlank()
     * @Assert\MinLength(limit="3")
     * @Assert\MaxLength(limit="255")
     */
    protected $username;

    /**
     * @MongoDB\String
     * @Assert\NotBlank()
     * @Assert\MinLength(limit="3")
     * @Assert\MaxLength(limit="255")
     */
    protected $password;

    /**
     * @MongoDB\Collection
     */
    protected $queue = array(1, 5, 2, 4, 3, 6, 8, 7, 14, 9, 16, 10, 17, 12, 18, 11, 13, 15);

    public function __toString()
    {
        return (string)$this->getUsername();
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setPassword($password)
    {
        $this->password = $password;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function setUsername($username)
    {
        $this->username = $username;
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function setQueue($queue)
    {
        $this->queue = $queue;
    }

    public function getQueue()
    {
        return $this->queue;
    }


}
